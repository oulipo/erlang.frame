-module(frame_templater).
-behaviour(gen_server).
%%%
%%%-------------------------------------
%%%  behaviour api
%%%
-export([init/1, handle_call/3, 
	 handle_cast/2, handle_info/2, 
	 terminate/2, code_change/3]).

-export([start_link/0]).
%% ----- External Interface -----
-export([load_templates/1]).
%%

-define(CHILD_SPEC(ID,Templates), #{ id => ID,
				     start => {ID, start_link, [Templates]},
				     restart => permanent,
				     type => worker })

init([])->
    {ok,[]}.

handle_call( _Call, From, State )->
    {reply, From, State }.

handle_cast( _Call, State )->
    {noreply, State }.

handle_info( _Call, State )->
    {noreply, State}.

terminate( _Reason, _State)->
    ok.

code_change(_A,_B,_C)->
    {noreply, _B, _C }.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

load_templates(Templates)->
    supervisor:start_child(frame_templater_sup, ?CHILD_SPEC(?MODULE, Templates)).
