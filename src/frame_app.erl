%%%-------------------------------------------------------------------
%% @doc frame public API
%% @end
%%%-------------------------------------------------------------------

-module(frame_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    frame_templater_sup:start_link(),
    frame_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
